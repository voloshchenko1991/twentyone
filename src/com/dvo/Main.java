package com.dvo;

import java.util.Random;

public class Main {


    public static void main(String[] args) throws InterruptedException {
        Card card;
        Deck<Card> deck = new Deck<>();
        int[] score = {0,0};
        int player = 0;
        while (true) {
            if(deck.checkEmptyDeck()){
                System.out.println("Game over! Deck is empty!");
                break;
            }
            System.out.println("Player " + (player + 1) + " makes his turn");
            Thread.sleep(1000);
            card = deck.getCard();
            score[player] += card.getPoints();
            System.out.println("Player " + (player + 1) + " get " + card.getRanks() + " " + card.getSuits() +
                    " points are: " + card.getPoints() + " and total score is: " + score[player]);
            System.out.println("------------------------------");
            if(score[player] > 21){
                System.out.println("Game over! Player " + player + " get over 21 and LOSE!");
                break;
            }else if(score[player] == 21){
                System.out.println("Game over! Player " + player + " get 21 and WIN!");
                break;
            }
            player = (player == 0)?1:0;
        }
    }


}
