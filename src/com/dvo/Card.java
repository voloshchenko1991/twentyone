package com.dvo;

/**
 * Created by User on 23.07.2017.
 */
public class Card {

    private Ranks ranks;
    private Suits suits;

    Card(Ranks ranks, Suits suits) {
        this.ranks = ranks;
        this.suits = suits;
    }

    public Ranks getRanks() {
        return ranks;
    }

    public Suits getSuits() {
        return suits;
    }


    public void setRanks(Ranks ranks) {
        this.ranks = ranks;
    }


    public void setSuits(Suits suits) {
        this.suits = suits;
    }

    public int getPoints() {
        int result = 0;
        switch (ranks) {
            case TWO:
                result = 2;
                break;
            case THREE:
                result = 3;
                break;
            case FOUR:
                result = 4;
                break;
            case FIVE:
                result = 5;
                break;
            case SIX:
                result = 6;
                break;
            case SEVEN:
                result = 7;
                break;
            case EIGHT:
                result = 8;
                break;
            case NINE:
                result = 9;
                break;
            case TEN:
                result = 10;
                break;
            case T:
                result = 1;
                break;
            case J:
                result = 11;
                break;
            case Q:
                result = 12;
                break;
            case K:
                result = 13;
                break;
            default:
                result = 0;
        }
        return result;
    }

}
