package com.dvo;

/**
 * Created by User on 23.07.2017.
 */
public enum Suits {
    DIAMOND,
    HEARTS,
    SPADES,
    CLUBS,
    EMPTY
}
