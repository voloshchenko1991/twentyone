package com.dvo;

/**
 * Created by User on 23.07.2017.
 */
public enum Ranks {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    J,
    Q,
    K,
    T,
    EMPTY
}
