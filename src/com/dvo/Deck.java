package com.dvo;

import java.util.Random;

/**
 * Created by User on 23.07.2017.
 */
public class Deck<T> {
    private boolean isGameOver = false;
    private Object[] deck;
    private final Card EMPTY_CARD = new Card(Ranks.EMPTY, Suits.EMPTY);
    Deck(){
        newGame();
    }


    private void newGame() {
        deck = (T[]) new Object[52];
        for (int i = 0; i < deck.length; i++) {
            deck[i] = EMPTY_CARD;
        }
        for (int i = 0; i < Ranks.values().length - 1; i++) {
            for (int j = 0; j < Suits.values().length - 1; j++) {
                Card card = new Card(Ranks.values()[i], Suits.values()[j]);
                deck[getRandomPlace()] = card;
            }
        }
    }

    private int getRandomPlace(){
        int randomPlace;
        Random random = new Random();
        do {
            randomPlace = random.nextInt(52);
        }while(!checkPlace(randomPlace));
        return randomPlace;
    }

    private boolean checkPlace(int place){
        return deck[place].equals(EMPTY_CARD)?true:false;
    }

    public T getCard(){
        T topCard = (T)EMPTY_CARD;
        for (int i = 0; i < deck.length; i++) {
            topCard = (T)deck[i];
             if(!topCard.equals(EMPTY_CARD)){
                deck[i] = (T)EMPTY_CARD;
                return topCard;
            }
        }
        isGameOver = true;
        return topCard;
    }

    public boolean checkEmptyDeck(){
        return isGameOver;
    }

}
